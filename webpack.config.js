const HtmlWebpackPlugin = require('html-webpack-plugin');

const path = require('path');
const config = {
    entry: {
        app: './index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].bundle.js",
    },
    devServer: {
        port: 3000,
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.pug',
            filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
            template: './src/about.pug',
            filename: 'about.html'
        }),
    ],
    module: {
        rules: [{
                test: /\.pug$/,
                use: ['pug-loader']
            }, {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            }

        ]
    },
    watch: true
};
module.exports = (env, argv) => {
    if (argv.mode === 'development') {}
    if (argv.mode === 'production') {}
    return config;
}